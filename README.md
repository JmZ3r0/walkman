# Walkman
Walkman Downloader

Herramienta sencilla para descargar audio de los video de Youtube. Escrita en Python

Requerimientos

Asegurese de tener las siguientes dependencias instaladas en python 3.x

i)pyqt5
ii)youtube_dl
iii)pafy

Instalación:

i)Para instalar pyqt5
pip install pyqt5

Para instalar youtube_dl
ii)pip install youtube_dl

Para instalar pafy
iii)pip install pafy

Descarge el proyecto desde el repositorio oficial:
git clone https://gitlab.com/JmZ3r0/walkman.git

Ejecución:

Para ejecutar el programa escriba la siguiente linea:

python win.py


